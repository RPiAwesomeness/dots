" Line numbers
set number relativenumber 

" Bug fix for fullscreen usage
set t_Co=256
set termguicolors
set encoding=UTF-8
filetype indent plugin on

" Clear search buffers
nnoremap <silent> <Leader><space> :let @/=""<cr>

" Remember open files
map <F2> :mksession! ~/.vim_session <cr> " Quick write session with F2
map <F3> :source ~/.vim_session <cr>     " And load session with F3

" Handle folding
filetype plugin indent on " required
syntax on                 " required

autocmd Filetype * AnyFoldActivate               " activate for all filetypes

set foldlevel=99  " open all folds

" Ruler
set colorcolumn=120

" Automatically load .ycm_extra_conf.py without prompting after first time
let g:ycm_confirm_extra_conf = 0

" NERDSpace config
" One space after comment
let g:NERDSpaceDelims = 1
" Allow commenting/inverting empty lines (good for blocks)
let g:NERDCommentEmptyLines = 1
" Enable NERDCommenterToggle to check all selected lines is commented or not 
let g:NERDToggleCheckAllLines = 1
" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" Toggle highlighted
nmap <silent> <C-_>   <Plug>NERDCommenterToggle
vmap <silent> <C-_>   <Plug>NERDCommenterToggle<CR>gv

" Toggle NERDTree
nmap <silent> <C-\> :NERDTreeToggle<Return>

" Split window
nmap ss :split<Return><C-w>w 
nmap sv :vsplit<Return><C-w>w

" Move window
nmap <Space> <C-w>w
map s<left> <C-w>h
map s<right> <C-w>l
map s<up> <C-w>k
map s<down> <C-w>j

map sh <C-w>h
map sl <C-w>l
map sk <C-w>k
map sj <C-w>j

" Resize window
nmap <C-w><left> <C-w><
nmap <C-w><right> <C-w>>
nmap <C-w><up> <C-w>+
nmap <C-w><down> <C-w>-

" Easier split nav
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Improve tabs
nnoremap <C-Tab> gt 
nnoremap <A-1> 1gt
nnoremap <A-2> 2gt
nnoremap <A-3> 3gt
nnoremap <A-4> 4gt
nnoremap <A-5> 5gt
nnoremap <A-6> 6gt

" Handle tab for different languages
set expandtab
autocmd FileType c    setlocal ts=2 sw=2 sts=2
autocmd FileType rust setlocal ts=2 sw=2 sts=2

" Autoclose YCM preview
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_autoclose_preview_window_after_completion = 1

" Ale error jumping
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)

" Configure linting
let g:ale_linters={
\   'c': ['clangtidy'],
\}
let g:ale_fixers={
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'c': ['clang-format'],
\}
let g:ale_c_clang_options='-Iinclude/'
let g:ale_cpp_clang_options='-Iinclude/'

" Format on save
function! Formatonsave()
  let l:formatdiff = 1
  pyf /usr/share/clang/clang-format-8/clang-format.py
endfunction
autocmd BufWritePre *.h,*.cc,*.cpp call Formatonsave()

" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.local/share/nvim/plugged')

" Make sure you use single quotes

" Folding
Plug 'pseewald/anyfold'

" Autocomplete
" if has('nvim')
  " Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
" else
  " Plug 'Shougo/deoplete.nvim'
  " Plug 'roxma/nvim-yarp'
  " Plug 'roxma/vim-hug-neovim-rpc'
" endif
" let g:deoplete#enable_at_startup = 1

" On-demand loading
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

" Fancy commenting!
Plug 'scrooloose/nerdcommenter'

if has('nvim')
  Plug 'Shougo/defx.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/defx.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif

function! BuildYCM(info)
    " info is a dictionary with 3 fields
    "   " - name:   name of the plugin
    "     " - status: 'installed', 'updated', or 'unchanged'
    "       " - force:  set on PlugInstall! or PlugUpdate!
  if a:info.status == 'installed' || a:info.force
    !./install.py --rust-completer --go-completer --clang-completer
        endif
endfunction
Plug 'Valloric/YouCompleteMe', { 'do': function('BuildYCM') }

" YCM-Generator
Plug 'rdnetto/YCM-Generator', { 'branch': 'stable'}

" Autocomplete/Linting with Neoclide
" Plug 'neoclide/coc.nvim', {'tag': '*', 'do': './install.sh'}

" Using a tagged release; wildcard allowed (requires git 1.9.2 or above)
Plug 'fatih/vim-go', { 'tag': '*' }

" Plugin options
Plug 'nsf/gocode', { 'tag': 'v.20150303', 'rtp': 'vim' }

" One Dark theme
Plug 'joshdick/onedark.vim'

" Status bar!
Plug 'itchyny/lightline.vim'

" Enable icons
Plug 'ryanoasis/vim-devicons'

" Linter - disabled because it's not updating and it's being silly
Plug 'w0rp/ale'

" Initialize plugin system
call plug#end()

" lightline config
let g:lightline = {
      \ 'colorscheme': 'onedark',
      \ }

" Enable syntax
syntax on
colorscheme onedark

