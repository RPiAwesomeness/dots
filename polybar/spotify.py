#!/usr/bin/env python3
# Handles setting the text for my polybar Spotify widget
# Adapted for Playerctl from https://github.com/Jvanrhijn/polybar-spotify

import gi

gi.require_version("Playerctl", "2.0")

from gi.repository import Playerctl, GLib

output = "{prefix} {artist}: {song}"
playing = ""
paused = ""
trunclen = 25

player = Playerctl.Player(player_name="spotify")

try:
    song = player.get_title()
    artist = player.get_artist()
    status = player.get_property("status")
except:
    pass
else:
    if song is None or song == "":
        print("")

    if len(song) > trunclen:
        song = song[0:trunclen] + "..."

    if status.startswith("Playing"):
        prefix = playing
    elif status.startswith("Paused"):
        prefix = paused
    
    print(output.format(prefix=prefix, artist=artist, song=song))
