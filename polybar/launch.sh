#!/usr/bin/env sh

# Kill all running bar instances
killall -q polybar

# Wait until all processes have terminated
while pgreg -x polybar > /dev/null; do sleep 1; done

# Relaunch polybar
polybar top &
polybar bottom &
