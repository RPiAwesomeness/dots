#!/usr/bin/env sh

xdotool getactivewindow getwindowname | egrep -q ".*Firefox$" || (notify-send $1 && i3-msg "workspace number $1")
