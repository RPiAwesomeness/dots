" Enable filetype detection
filetype on

" Enable line numbers
set number

" Default indentation
set shiftwidth=4
set tabstop=4
set softtabstop=4

" Expand new tabs to spaces
set expandtab

" Add character line at line 80
set cc=80
highlight ColorColumn ctermbg=7

set termguicolors
set encoding=UTF-8

" Key remappings
nnoremap ; :
imap jk <Esc>
imap jj <Esc>

" Move/Resize Window
nmap <Space> <C-w>w
map s<left> <C-w>h
map s<right> <C-w>l
map s<up> <C-w>k
map s<down> <C-w>j

map sh <C-w>h
map sl <C-w>l
map sk <C-w>k
map sj <C-w>j

nmap <C-w><left> <C-w><
nmap <C-w><right> <C-w>>
nmap <C-w><up> <C-w>+
nmap <C-w><down> <C-w>-

" Easier split nav
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

call plug#begin('~/.vim/plugged')

Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }

" Status Bar
Plug 'itchyny/lightline.vim'

" Nord theme
Plug 'arcticicestudio/nord-vim'

call plug#end()

" Activate nord theme
syntax on
colorscheme nord

let g:lightline = { 'colorscheme': 'nord' }

