# D O T S

All my dotfiles! Feel free to use any of them!

## Currently Present
### Configs
 * i3-gaps
 * polybar
 * compton
 * cava
 * termite
 * dunst
 * rofi

### Backgrounds
 * Minimal backgrounds pulled from around the internet off of Google. Attribution forthcoming

### Screenshots

![neofetch screenshot](screenshots/neofetch.png)
